export const INCLUDE_REGEX = /^\s*!include\s+(.*\.(plantuml|pu|puml|wsd))\s*$/i;
export const INCLUDESUB_REGEX = /^\s*!includesub\s+(.*\.(plantuml|pu|puml|wsd))!(.*)\s*$/i;

export const STARTSUB_REGEX = /^\s*!startsub\s+(.*)\s*$/i;
export const ENDSUB_REGEX = /^\s*!endsub\s*$/i;
